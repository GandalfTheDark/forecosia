package com.example.ecosiaproject;

public class singleTrack {
    String name, path, artist, album;

    public singleTrack(String _name, String _path, String _artist, String _album){
        this.album = _album;
        this.path = _path;
        this.artist = _artist;
        this.name = _name;
    }

    public String getName() {
        return name;
    }

    public String getAlbum() {
        return album;
    }

    public String getArtist() {
        return artist;
    }

    public String getPath() {
        return path;
    }
}
