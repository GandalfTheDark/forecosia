package com.example.ecosiaproject;

import android.app.Service;
import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.provider.MediaStore;
import android.util.Log;
import android.widget.RemoteViews;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collections;

public class TrackPlayerService extends Service {
    private static final String TAG = "TrackPlayerService";
    MediaPlayer myPlayer;
    ArrayList<singleTrack> song_paths = new ArrayList<>();
    Boolean alreadyStarted = false;
    Handler handler = new Handler();
    int track_duration = 0;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    public int onStartCommand(Intent intent, int flags, int startId) {

        // we double check permissions
        checkForPermission();

        return Service.START_NOT_STICKY;
    }

    private void checkForPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(android.Manifest.permission.READ_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED) {

                Log.e(TAG, "Please open app and grant me permission");
                Toast.makeText(getApplicationContext(),"Please open app and grant me permission",Toast.LENGTH_LONG).show();

                Intent appOpener = new Intent(this, MainActivity.class);
                appOpener.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(appOpener);

                return;
            }else{ // permission granted
                findAllAudiosOnDevice();
            }
        }else{
            findAllAudiosOnDevice();
            Log.e(TAG, "We dont need permission, continue");
        }
    }

    public void findAllAudiosOnDevice() {

        if(alreadyStarted && (myPlayer != null && myPlayer.isPlaying())){
            myPlayer.pause();

            if (handler != null)
                    handler.removeCallbacks(updateProgress);

            RemoteViews view = new RemoteViews(getPackageName(), R.layout.ecosia_player);
            ComponentName theWidget = new ComponentName(this, ecosia_player.class);
            AppWidgetManager manager = AppWidgetManager.getInstance(this);
            view.setTextViewText(R.id.duration_field, "Paused");
            manager.updateAppWidget(theWidget, view);

            Toast.makeText(getApplicationContext(),"Paused",Toast.LENGTH_SHORT).show();

        }else{

            if(myPlayer != null){
                myPlayer.start();

                handler.post(updateProgress);
                Toast.makeText(getApplicationContext(),"Play",Toast.LENGTH_SHORT).show();

            }else{

                Uri uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                String[] projection = {MediaStore.Audio.AudioColumns.DATA, MediaStore.Audio.AudioColumns.ALBUM, MediaStore.Audio.ArtistColumns.ARTIST};
                Cursor c = getContentResolver().query(uri, projection, null, null, null);

                if (c != null) {
                    while (c.moveToNext()) {
                        String path = c.getString(0);
                        String album = c.getString(1);
                        String artist = c.getString(2);
                        String name = path.substring(path.lastIndexOf("/") + 1);

                        if(path.endsWith(".mp3")){ // the audio file is definitely an MP3
                            song_paths.add(new singleTrack(name, path, artist, album));
                        }
                    }
                    c.close();
                }

                randomizeSongTrack();
            }

        }

    }

    private void randomizeSongTrack() {
        if(song_paths.size() == 0){
            Toast.makeText(getApplicationContext(),"Sorry, there is not mp3 files on this device",Toast.LENGTH_SHORT).show();
        }else{
            Collections.shuffle(song_paths);

            // start playing
            myPlayer = MediaPlayer.create(this, Uri.parse(song_paths.get(0).getPath()));
            myPlayer.setLooping(false);
            myPlayer.start();
            alreadyStarted = true;

            track_duration = myPlayer.getDuration();

            int minutes = track_duration /  (60 * 1000);
            int seconds = track_duration %  (60 * 1000) / 1000;

            RemoteViews view = new RemoteViews(getPackageName(), R.layout.ecosia_player);
            ComponentName theWidget = new ComponentName(this, ecosia_player.class);
            AppWidgetManager manager = AppWidgetManager.getInstance(this);
            view.setTextViewText(R.id.duration_field, minutes +" : " + seconds);
            view.setTextViewText(R.id.name_artist_field, song_paths.get(0).getName() +" - "+song_paths.get(0).getArtist());
            view.setTextViewText(R.id.album_field, song_paths.get(0).getAlbum());
            manager.updateAppWidget(theWidget, view);

            handler.post(updateProgress);
        }
    }

        Runnable updateProgress = new Runnable() {
        @Override
        public void run() {
            track_duration -= 1000;
            if(track_duration < 0){
                handler.removeCallbacks(updateProgress);
            }else{
                int minutes = (track_duration /  (60 * 1000));
                int seconds = (track_duration %  (60 * 1000)) / 1000;

                RemoteViews view = new RemoteViews(getPackageName(), R.layout.ecosia_player);
                ComponentName theWidget = new ComponentName(getBaseContext(), ecosia_player.class);
                AppWidgetManager manager = AppWidgetManager.getInstance(getBaseContext());
                view.setTextViewText(R.id.duration_field, minutes +" : " + seconds);
                manager.updateAppWidget(theWidget, view);

                handler.postDelayed(updateProgress, 1000);
            }
        }
    };

    public void onDestroy() {
        if(myPlayer != null){
            myPlayer.stop();
            myPlayer.release();
        }

        if (handler != null)
            handler.removeCallbacks(updateProgress);

        stopSelf();
        super.onDestroy();
    }
}
